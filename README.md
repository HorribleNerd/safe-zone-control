# Safe Zone Control

Commands:

- Toggle
- Enable
- Disable
- Range [float]
- Increase [float]
- Decrease [float]
- Color [float] [float] [float]
- RandomColor
- GradientNext [optional int stepsize]