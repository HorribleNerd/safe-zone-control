// HorribleNerd's Safe Zone Control
// Commands:
// - Toggle
// - Enable
// - Disable
// - Range [float amount]
// - Increase [float amount]
// - Decrease [float amount]
// - Color [float r] [float g] [float b]
// - RandomColor
// - GradientNext [optional int stepsize]



List<IMySafeZoneBlock> zones = new List<IMySafeZoneBlock>();

public void Main(string argument, UpdateType updateSource) {

    string[] args = argument.Split(' ');

    GridTerminalSystem.GetBlocksOfType(zones);
    if (zones == null || !zones.Any()) {
        Echo("No valid Safe Zone found.");
        return;
    }
    foreach (IMySafeZoneBlock zone in zones) {
        switch (args[0]) {
            case "Toggle":
                zone.SetValue("SafeZoneCreate", true);
                break;
            case "Enable":
                if (!zone.GetValue<bool>("SafeZoneCreate")) {
                    zone.SetValue("SafeZoneCreate", true);
                }
                break;
            case "Disable":
                if (zone.GetValue<bool>("SafeZoneCreate")) {
                    zone.SetValue("SafeZoneCreate", false);
                }
                break;
            case "Range":
                zone.SetValue("SafeZoneSlider", float.Parse(args[1]));
                break;
            case "Increase":
                zone.SetValue("SafeZoneSlider", zone.GetValue<float>("SafeZoneSlider") + float.Parse(args[1]));
                break;
            case "Decrease":
                zone.SetValue("SafeZoneSlider", zone.GetValue<float>("SafeZoneSlider") - float.Parse(args[1]));
                break;
            case "Color":
            case "Colour":
                if (args.Count() != 4) {
                    Echo("Invalid number of arguments.");
                    return;
                }
                zone.SetValue("SafeZoneColor", new Color(float.Parse(args[1]), float.Parse(args[2]), float.Parse(args[3])));
                break;
            case "RandomColor":
            case "RandomColour":
                Random rand = new Random();
                int r = rand.Next(0, 255);
                int g = rand.Next(0, 255);
                int b = rand.Next(0, 255);
                zone.SetValue("SafeZoneColor", new Color(r,g,b));
                break;
            case "GradientNext":
                int step = 1;
                if (args.Count() == 2) step = int.Parse(args[1]);

                Color c = zone.GetValue<Color>("SafeZoneColor");
                int newR = c.R;
                int newG = c.G;
                int newB = c.B;
                if (newR > 0 && newB == 0) {
                    newR -= step;
                    newG += step;
                }
                else if (newG > 0) {
                    newG -= step;
                    newB += step;
                }
                else if (newB > 0) {
                    newB -= step;
                    newR += step;
                }

                if (newR < 0) newR = 0;
                else if (newR > 255) newR = 255;

                if (newG < 0) newG = 0;
                else if (newG > 255) newG = 255;
                
                if (newB < 0) newB = 0;
                else if (newB > 255) newB = 255;

                zone.SetValue("SafeZoneColor", new Color(newR,newG,newB));
                break;
            default:
                Echo("Invalid argument: " + args[0]);
                return;
        }
        Echo("Done.");
    }
}